import React from 'react';

const HeadComponents = [
  <script key="some-key" dangerouslySetInnerHTML={{__html: `
      /* copy and paste you js script here */
  `}}/>,
  <script key="jquery" src="/adminlte/plugins/jquery/jquery.min.js"/>
];
const BodyComponents = [
  <script key="bootstrap" src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"/>,
  <script key="adminlte" src="/adminlte/dist/js/adminlte.min.js"></script>,
  <script key="dataTable" src="/adminlte/plugins/datatables/jquery.dataTables.min.js"/>
]

const onRenderBody = ({ setHeadComponents, setPostBodyComponents }) => {
  setHeadComponents(HeadComponents);
  setPostBodyComponents(BodyComponents);
   
};

export { onRenderBody };
import React from "react";
import Layout from "./components/layouts.js";

export default function Home() {
  return (
    <Layout>
      <section>
        <div>
          <button className="btn btn-sm btn-success">button</button>
        </div>
      </section>
    </Layout>
  )
}